<?php

namespace Offpak\Hello;

class HelloManager
{
    /**
     * @var string
     */
    private $name;

    /**
     * HelloManager constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }


    public function hello()
    {
        return "HELLO {$this->name}!";
    }
}